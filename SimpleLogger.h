/***
 * Basic SimpleLogger Library for C
 *
 * ~Overflwn @ 2022
 */

#ifndef LOGGER_H
#define LOGGER_H
#include "ExportDefines.h"
#include <stdio.h>
// Some default C/C++ standard macros
// __LINE__, __FILE__
// __DATE__, __TIME__ (NOTE: Both are the time of translation of the source file

#define LOGTYPE_INFO 0
#define LOGTYPE_WARN 1
#define LOGTYPE_ERROR 2
#define LOGTYPE_DEBUG 3

#define LOGMODE_ERRORS 0
#define LOGMODE_WARNINGS 1
#define LOGMODE_DEBUG 2
#define LOGMODE_ALL 3

/***
 * Initialize SimpleLogger, e.g. opens filehandles for example.
 * @return 0 on success, error otherwise
 */
SIMPLELOGGER int SimpleLogger_Init(int mode, const char* logsPath);

/***
 * The main log function.
 * @param type LOGTYPE_INFO, [...]_WARN, [...]_ERROR or [...]_DEBUG
 * @param message The main message, optionally with standard format options.
 * @param ... Optional format parameters
 * @return 0 on success, error otherwise
 */
SIMPLELOGGER int SimpleLogger_Log(int type, const char* message, ...);

SIMPLELOGGER void SimpleLogger_SetMode(int mode);

#endif