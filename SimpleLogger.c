#include "SimpleLogger.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>

#define BOLD_RED "\033[1;31m"
#define RED "\033[0;31m"
#define PURPLE "\033[0;35m"
#define BOLD_PURPLE "\033[1;35m"
#define WHITE "\033[0;37m"
#define BOLD_WHITE "\033[1;37m"
#define CYAN "\033[0;36m"

struct SimpleLogger {
    char logsPath[150];
    int mode;
} SimpleLogger_Settings;
typedef struct SimpleLogger SimpleLogger;

static FILE* logFile = NULL;

static void clearOldLogs() {
    //TODO
}

void SimpleLogger_SetMode(int mode) {
    if(mode == LOGMODE_ALL || mode == LOGMODE_DEBUG || mode == LOGMODE_ERRORS || mode == LOGMODE_WARNINGS) {
        SimpleLogger_Settings.mode = mode;
    }else {
        SimpleLogger_Settings.mode = LOGMODE_WARNINGS;
    }
}

int SimpleLogger_Init(int mode, const char* logsPath) {
    sprintf(SimpleLogger_Settings.logsPath, "%s", logsPath);

    printf("Initializing SimpleLogger.\n");
    time_t now = time(NULL);

    char text[200];
    char fullPath[400];
    struct tm* t = localtime(&now);
    strftime(text, sizeof(text), "%Y-%m-%d_%H-%M.log", t);
    printf("Log file: %s\n", text);
    struct stat st = {0};
    if(stat(SimpleLogger_Settings.logsPath, &st) == -1) {
        perror("Error initializing SimpleLogger: ");
        return 1;
    }
    sprintf(fullPath, "%s%s", SimpleLogger_Settings.logsPath, text);
    logFile = fopen(fullPath, "w");
    if(logFile == NULL) {
        perror("Error opening logfile: ");
        return 1;
    }
    printf("Logfile open.\n");

    SimpleLogger_SetMode(mode);

    printf("Logger finished initalizing.\n");
    return 0;
}

static int log(int type, const char* message) {
    char text[100];
    time_t now = time(NULL);

    struct tm* t = localtime(&now);
    strftime(text, sizeof(text), "%Y-%m-%d %H:%M", t);

    int ret = 0;
    int printIt = 0;

    if(logFile != NULL) {

        switch(type) {
            case LOGTYPE_ERROR:
                printf("[%s] ", text);
                if(fprintf(logFile, "[%s] ", text) < 0) {
                    perror("COULD NOT WRITE TO LOG FILE: ");
                    ret = 1;
                }
                printf(BOLD_RED);
                printf("[ERROR] ");
                if(fprintf(logFile, "[ERROR] ") < 0) {
                    perror("COULD NOT WRITE TO LOG FILE: ");
                    ret = 1;
                }
                printIt = 1;
                break;
            default:
            case LOGTYPE_INFO:
                if(fprintf(logFile, "[%s] ", text) < 0) {
                    perror("COULD NOT WRITE TO LOG FILE: ");
                    ret = 1;
                }
                if(fprintf(logFile, "[INFO] ") < 0) {
                    perror("COULD NOT WRITE TO LOG FILE: ");
                    ret = 1;
                }
                if(SimpleLogger_Settings.mode == LOGMODE_ALL || SimpleLogger_Settings.mode == LOGMODE_DEBUG) {
                    printf("[%s] ", text);
                    printf("[INFO] ");
                    printIt = 1;
                }
                break;
            case LOGTYPE_WARN:
                if(fprintf(logFile, "[%s] ", text) < 0) {
                    perror("COULD NOT WRITE TO LOG FILE: ");
                    ret = 1;
                }
                if(fprintf(logFile, "[WARN] ") < 0) {
                    perror("COULD NOT WRITE TO LOG FILE: ");
                    ret = 1;
                }
                if(SimpleLogger_Settings.mode == LOGMODE_WARNINGS || SimpleLogger_Settings.mode == LOGMODE_ALL || SimpleLogger_Settings.mode == LOGMODE_DEBUG) {
                    printf("[%s] ", text);

                    printf(BOLD_PURPLE);
                    printf("[WARN] ");

                    printIt = 1;
                }
                break;
            case LOGTYPE_DEBUG:
                if(fprintf(logFile, "[%s] ", text) < 0) {
                    perror("COULD NOT WRITE TO LOG FILE: ");
                    ret = 1;
                }
                if(fprintf(logFile, "[DEBUG] ") < 0) {
                    perror("COULD NOT WRITE TO LOG FILE: ");
                    ret = 1;
                }
                if(SimpleLogger_Settings.mode == LOGMODE_DEBUG) {
                    printf("[%s] ", text);

                    printf(CYAN);
                    printf("[DEBUG] ");

                    printIt = 1;
                }

                break;
        }
    }else {
        printf("WARNING: Logger is not initialized!\n");

        switch(type) {
            case LOGTYPE_ERROR:
                printf("[%s] ", text);
                printf(BOLD_RED);
                printf("[UNINITIALIZED:ERROR] ");
                break;
            default:
            case LOGTYPE_INFO:
                if(SimpleLogger_Settings.mode == LOGMODE_ALL || SimpleLogger_Settings.mode == LOGMODE_DEBUG) {
                    printf("[%s] ", text);
                    printf(WHITE);
                    printf("[UNINITIALIZED:INFO] ");
                    printIt = 1;
                }

                break;
            case LOGTYPE_WARN:
                if(SimpleLogger_Settings.mode == LOGMODE_WARNINGS || SimpleLogger_Settings.mode == LOGMODE_ALL || SimpleLogger_Settings.mode == LOGMODE_DEBUG) {
                    printf("[%s] ", text);
                    printf(BOLD_PURPLE);
                    printf("[UNINITIALIZED:WARN] ");
                    printIt = 1;
                }

                break;
            case LOGTYPE_DEBUG:
                if(SimpleLogger_Settings.mode == LOGMODE_DEBUG) {
                    printf("[%s] ", text);
                    printf(CYAN);
                    printf("[UNINITIALIZED:DEBUG] ");
                    printIt = 1;
                }

                break;

        }

    }
    if(printIt == 1) {
        printf("%s\n", message);
        printf(WHITE);

    }
    if(logFile != NULL
       && (type != LOGTYPE_DEBUG || SimpleLogger_Settings.mode == LOGMODE_DEBUG)
       && fprintf(logFile, "%s\n", message) < 0) {
        perror("COULD NOT WRITE TO LOG FILE: ");
        ret = 1;
    }

}

int SimpleLogger_Log(int type, const char* message, ...) {
    va_list vl;
    va_list copy;

    va_start(vl, message);
    va_copy(copy, vl);

    int len = vsnprintf(NULL, 0, message, vl);
    va_end(vl);
    // with \0
    char buf[len+1];
    char* buf_test = (char*)malloc(sizeof(char)*(len+1));
    //printf("Buf Len: %d, message: %s\n", len+1, message);

    vsnprintf(buf_test, len+1, message, copy);

    va_end(copy);
    //printf("Buf: %s\n", buf_test);
    int ret =  log(type, buf_test);
    free(buf_test);
    return ret;
}