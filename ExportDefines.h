#ifndef EXPORT_DEFINES_H
#define EXPORT_DEFINES_H
#if defined(_MSC_VER)
#if defined(SIMPLELOGGER_EXPORT)
        #if defined(__cplusplus)
            #define SIMPLELOGGER extern "C" __declspec(dllexport)
        #else
            #define SIMPLELOGGER __declspec(dllexport)
        #endif
    #else
        #if defined(__cplusplus)
            #define SIMPLELOGGER extern "C" __declspec(dllimport)
        #else
            #define SIMPLELOGGER __declspec(dllimport)
        #endif
    #endif
#elif defined(__GNUC__)
#if defined(SIMPLELOGGER_EXPORT)
#if defined(__cplusplus)
#define SIMPLELOGGER extern "C" __attribute__((visibility("default")))
#else
#define SIMPLELOGGER __attribute__((visibility("default")))
#endif
#else
#if defined(__cplusplus)
            #define SIMPLELOGGER extern "C"
        #else
            #define SIMPLELOGGER
        #endif
#endif
#else // something unknown

#if defined(__cplusplus)
        #define SIMPLELOGGER extern "C"
    #else
        #define SIMPLELOGGER
    #endif
    #pragma warning Unknown dynamic link import/export semantics.

#endif

#endif